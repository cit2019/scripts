#!/bin/sh

# USAGE: 04-join-swarm-as-worker.sh <@ip_leader_swarm>

sudo apt-get install sshpass
sshpass -p 1234 scp pi@$1:/scripts/token_worker.txt /script/token_worker.txt
read token_worker < token_worker.txt

docker swarm join --token token_worker $1:2377

