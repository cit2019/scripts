#!/bin/sh

# USAGE: 05-join-swarm-as-manager.sh <@ip_leader_swarm>

sudo apt-get install sshpass
sshpass -p 1234 scp pi@$1:/scripts/token_manager.txt /script/token_manager.txt
read token_manager < token_manager.txt

docker swarm join --token token_manager $1:2377


