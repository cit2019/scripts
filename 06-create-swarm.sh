#!/bin/sh
# USAGE: 03-create-swarm.sh <@ip>
# RETURN token_worker.txt with token to join the swarm as worker
#RETURN token_manager.txt with token to join the swarm as manager

docker swarm init --advertise-addr $1 > token.txt

for mot in $(cat token.txt); do echo "$mot" >> test.txt ; done
rm token.txt
echo $(grep 'SWM' test.txt) >> token_worker.txt
rm test.txt
echo $(cat token_worker.txt)


docker swarm join-token manager > token.txt

for mot in $(cat token.txt); do echo "$mot" >> test.txt ; done
rm token.txt
echo $(grep 'SWM' test.txt) >> token_manager.txt
rm test.txt
echo $(cat token_manager.txt)




